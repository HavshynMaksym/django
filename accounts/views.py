from accounts.forms import AccountCreateForm, AccountPasswordChangeForm, AccountProfileUpdateForm, AccountUpdateForm
from accounts.models import Profile, UserActions

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.http import HttpResponseRedirect

from django.shortcuts import render # noqa
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView # noqa
from django.views.generic.edit import ProcessFormView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Account successfully created!')

        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)
        user = form.get_user()
        login = UserActions.USER_ACTION.LOGIN
        action = UserActions.objects.create(user=user, action=login)
        action.save()
        request = self.request
        messages.success(request, f'{user}, hello to LMS!')
        try:
            profile = self.request.user.profile
        except BaseException:
            profile = Profile.objects.create(user=self.request.user)
            profile.save()
        return result


class AccountLogoutView(LogoutView):
    template_name = 'logout.html'

    def get_template_names(self):
        result = super().get_template_names()
        request = self.request
        messages.info(request, 'Goodbye!')
        return result


# class AccountUpdateView(LoginRequiredMixin, UpdateView):
#     model = User
#     template_name = 'profile.html'
#     form_class = AccountUpdateForm
#     success_url = reverse_lazy('core:index')
#
#     def get_object(self, queryset=None):
#         return self.request.user
#
#     def form_valid(self, form):
#         result = super().form_valid(form)
#         request = self.request
#         messages.info(request, 'Profile successfully updated')
#         return result


class AccountPasswordChangeView(PasswordChangeView):
    form_class = AccountPasswordChangeForm
    template_name = 'change-password.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Password successfully updated ')
        return result


class AccountUpdateView(LoginRequiredMixin, ProcessFormView):
    # model = User
    # template_name = 'profile.html'
    # form_class = AccountUpdateForm
    # success_url = reverse_lazy('core:index')

    def get_object(self, queryset=None):
        return self.request.user

    def get(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile
        user_form = AccountUpdateForm(instance=user)
        profile_form = AccountProfileUpdateForm(instance=profile)

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,

            }
        )

    def post(self, request, *args, **kwargs):
        user = self.request.user
        profile = self.request.user.profile

        user_form = AccountUpdateForm(
            data=request.POST,
            instance=user
        )
        profile_form = AccountProfileUpdateForm(
            data=request.POST,
            files=self.request.FILES,
            instance=profile
        )

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

            old_img = profile_form.initial['image'].name
            new_img = profile.image.name

            if new_img != old_img:

                # new_img = request.FILES['image'].name

                image = UserActions.USER_ACTION.CHANGE_PROFILE_IMAGE
                action = UserActions.objects.create(user=user, action=image, info=f'{old_img} -> {new_img}')
                action.save()

                request = self.request
                messages.info(request, 'Profile successfully updated')
                return HttpResponseRedirect(reverse('accounts:profile'))

            change_profile = UserActions.USER_ACTION.CHANGE_PROFILE
            action = UserActions.objects.create(user=user, action=change_profile)
            action.save()

            return HttpResponseRedirect(reverse('accounts:profile'))

        return render(
            request=request,
            template_name='profile.html',
            context={
                'user_form': user_form,
                'profile_form': profile_form,
            }
        )
