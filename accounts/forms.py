from accounts.models import Profile

from django.contrib.auth.forms import PasswordChangeForm, UserChangeForm, UserCreationForm
from django.contrib.auth.models import User # noqa
from django.forms import ModelForm # noqa


class AccountCreateForm(UserCreationForm):
    pass
    # class Meta:
    #     model = User
    #     fields = ['username', 'first_name', 'last_name']


class AccountUpdateForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        fields = ['username', 'first_name', 'last_name']


class AccountPasswordChangeForm(PasswordChangeForm):
    pass


class AccountProfileUpdateForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['image', 'interests']
