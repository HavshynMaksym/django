from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect # noqa
from django.shortcuts import get_object_or_404, render # noqa
from django.urls import reverse, reverse_lazy # noqa
from django.views.generic import CreateView, DeleteView, ListView, UpdateView


from teachers.forms import TeacherCreateForm, TeacherEditForm
from teachers.models import Teacher


# Create your views here.


# def get_teachers(request):
#     teachers = Teacher.objects.all()
#
#     params = [
#
#     ]
#
#     for param in params:
#         value = request.GET.get(param)
#         if value:
#             teachers = teachers.filter(**{param: value})
#
#     return render(
#         request=request,
#         template_name='teachers.html',
#         context={
#             'teachers': teachers
#         }
#     )
#
#
# def create_teachers(request):
#
#     if request.method == 'GET':
#
#         form = TeacherCreateForm()
#
#     elif request.method == 'POST':
#
#         form = TeacherCreateForm(request.POST)
#
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('teachers:list'))
#
#     return render(
#         request=request,
#         template_name='teacher_create.html',
#         context={
#             'form': form
#         }
#     )
#
#
# def edit_teachers(request, id): # noqa
#     try:
#         teacher = Teacher.objects.get(id=id)
#     except Teacher.DoesNotExist:
#         return HttpResponse("Teachers doesn't exist", status=404)
#
#     if request.method == 'GET':
#
#         form = TeacherCreateForm(instance=teacher)
#
#     if request.method == 'POST':
#
#         form = TeacherCreateForm(
#             data=request.POST,
#             instance=teacher
#         )
#
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('teachers:list'))
#
#     return render(
#         request=request,
#         template_name='teacher_edit.html',
#         context={
#             'form': form,
#             'teacher': teacher,
#             'groups': teacher.groups.all(),
#         }
#     )
#
#
# def delete_teacher(request, id): # noqa
#     teacher = get_object_or_404(Teacher, id=id)
#
#     teacher.delete()
#     return HttpResponseRedirect(reverse('teachers:list'))


# TODO don't show age
class TeacherUpdateView(LoginRequiredMixin, UpdateView):
    model = Teacher
    form_class = TeacherEditForm
    template_name = 'teacher_edit.html'
    success_url = reverse_lazy('teachers:list')
    context_object_name = 'teacher'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['groups'] = self.get_object().groups.all()
        return context

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Teacher successfully updated')
        return result


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    form_class = TeacherCreateForm
    template_name = 'teacher_create.html'
    success_url = reverse_lazy('teachers:list')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Teacher successfully created')
        return result


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = 'teachers.html'
    context_object_name = 'teachers'
    paginate_by = 10

    def get_queryset(self):
        qs = super().get_queryset()
        request = self.request

        params = [
            'first_name',
            'last_name',
            'age',
            'experience',
            'job',
        ]

        for param in params:
            value = request.GET.get(param)
            if value:
                qs = qs.filter(**{param: value})

        return qs


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    template_name = 'teacher_delete.html'
    success_url = reverse_lazy('teachers:list')
    pk_url_kwarg = 'id'

    def get_success_url(self):
        obj = super().get_success_url()
        request = self.request
        messages.success(request, 'Teacher successfully deleted')
        return obj
