from django.urls import path

from teachers.views import TeacherCreateView,  TeacherDeleteView, TeacherListView, TeacherUpdateView

app_name = 'teachers'


urlpatterns = [
    path('', TeacherListView.as_view(), name='list'),
    path('create/', TeacherCreateView.as_view(), name='create'),
    path('edit/<int:id>', TeacherUpdateView.as_view(), name='edit'),
    path('delete/<int:id>', TeacherDeleteView.as_view(), name='delete')
]
