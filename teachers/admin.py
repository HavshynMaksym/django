from django.contrib import admin

from groups.models import Group

from teachers.models import Teacher


class GroupTable(admin.TabularInline):
    model = Group
    fields = ['name', 'rating', 'curator']
    readonly_fields = fields
    show_change_link = True
    fk_name = 'teacher'
    list_select_related = 'curator'


class TeacherAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'curated_group']
    fields = ['first_name', 'last_name']
    inlines = [GroupTable]


admin.site.register(Teacher, TeacherAdmin)
