from core.models import Person

from django.db import models

from faker import Faker


class Teacher(Person):
    experience = models.TextField(max_length=250, null=False)
    job = models.CharField(max_length=64, null=False)

    def __str__(self):
        return f'{super().__str__()}, ' \
               f'Experience: {self.experience};' \
               f'Place of work: {self.job}'

    @classmethod
    def create_teacher(cls):
        fake = Faker()

        first_name = fake.first_name()
        last_name = fake.last_name()
        birthdate = fake.date_of_birth()
        teacher = Teacher.objects.create(first_name=first_name, last_name=last_name, birthdate=birthdate)
        return teacher
