from django.core.management.base import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    help = 'Create random teachers' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        for _ in range(options['count']):
            teachers = Teacher.create_teacher()

        self.stdout.write(str(teachers))
