from django import forms

from teachers.models import Teacher


class TeacherBaseForm(forms.ModelForm):
    class Meta:
        model = Teacher
        # fields = ['first_name', 'last_name', 'job']
        fields = '__all__'
        # exclude = ['job']


class TeacherCreateForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        fields = '__all__'


class TeacherEditForm(TeacherBaseForm):
    class Meta(TeacherBaseForm.Meta):
        fields = '__all__'
