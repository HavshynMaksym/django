class Shape:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Point(Shape):
    def __init__(self, x, y):
        super().__init__(x, y)
        self.x = x
        self.y = y


class Circle(Shape):
    def __init__(self, center_x, center_y, radius):
        super().__init__(center_x, center_y)
        self.center_x = center_x
        self.center_y = center_y
        self.radius = radius

    def has_point(self, point):
        point_x = point.x
        point_y = point.y

        h = ((point_x - self.center_x)**2 + (point_y - self.center_y)**2)**0.5
        if h > self.radius:
            return False
        else:
            return True

    def __contains__(self, point):
        if type(point) == Point:
            b = self.has_point(point)
            return b
        else:
            raise TypeError('Enter Point')


c1 = Circle(10, 10, 5)
c2 = Circle(0, 20, 7)

p1 = Point(9, 9)
p2 = Point(10, -5)

# print(c1.has_point(p1), c1.has_point(p2))
# print(c2.has_point(p1), c2.has_point(p2))

s = Shape(9, 9)

print(p1 in c1)
