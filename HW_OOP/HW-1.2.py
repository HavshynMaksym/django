class Robot:
    company = "BostonDynamics"
    gear = "electric"
    model = 1.0

    def __init__(self, model):
        self.model = model
        self.battery_ma = 10000

    def charging(self, charge):
        self.battery_ma += charge


class SpotMini(Robot):
    weight_kg = 29

    def speed(self, time_h):
        print(f' working time: {time_h / 8}')


class Atlas(Robot):
    height_cm = 150

    def distance(self, time_h):
        print(f' working time: {time_h / 10}')


class Handle(Robot):
    length_cm = 120

    def payload(self, time_h):
        print(f' working time: {time_h / 4}')


r = Robot
print(r.company)

s = SpotMini(1.0)
print(s.weight_kg)

s = SpotMini(1.1)
s.speed(50)

a = Atlas(1.2)
a.distance(50)

h = Handle(1.4)
h.payload(50)

r = Robot(1)
r.charging(200)
print(r.battery_ma)
