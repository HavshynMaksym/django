from django.core.management.base import BaseCommand

from groups.models import Group


class Command(BaseCommand):
    help = 'Create random groups' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        for _ in range(options['count']):
            groups = Group.create_group()

        self.stdout.write(str(groups))
