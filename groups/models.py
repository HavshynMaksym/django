import datetime
import random

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

import students.models # noqa

from teachers.models import Teacher


class Classroom(models.Model):
    name = models.CharField(max_length=32)
    floor = models.PositiveSmallIntegerField(
        validators=[MinValueValidator(2), MaxValueValidator(5)]
    )

    def __str__(self):
        return f' name: {self.name};' \
               f' floor: {self.floor};'


class Group(models.Model):
    name = models.CharField(max_length=64, null=False)
    members = models.CharField(max_length=64, null=False)
    start_date = models.DateField(null=True, default=datetime.date(year=2020, month=8, day=4))
    end_date = models.DateField(null=True, default=datetime.date(year=2020, month=12, day=20))
    rating = models.IntegerField(null=True, default=0)
    teacher = models.ForeignKey(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='groups'
    )
    curator = models.OneToOneField(
        to=Teacher,
        null=True,
        on_delete=models.SET_NULL,
        related_name='curated_group'
    )
    headman = models.OneToOneField(
        to='students.Student',
        null=True,
        on_delete=models.SET_NULL,
        related_name='handled_group'
    )
    classrooms = models.ManyToManyField(
        to=Classroom,
        related_name='groups'
    )

    def __str__(self):
        return f'{self.id}) ' \
               f'Group name: {self.name};'
        # f' Members: {self.members}; ' \
        # f'Group start: {self.start_date};' \
        # f'Group end: {self.end_date};' \
        # f' Rating: {self.rating}'

    @classmethod
    def create_group(cls):
        course_list = [
            'Python for beginner',
            'Advanced python',
            'Python for ML',
            'Python for web',
            'Python for games',
            'Python for data science',
            'Python for hacking'
        ]
        name = random.choice(course_list)
        rating = random.randint(0, 100)
        teachers = Teacher.objects.only('id').all()
        random_teacher = random.choice(teachers)
        group = Group.objects.create(name=name, rating=rating, teacher=random_teacher)
        return group

    def clean_headman(self):
        headman = self.headman
        group = self.students.all()
        if headman in group:
            return headman
        else:
            raise ValidationError('Headman from another group')

    def save(self, *args, **kwargs):
        self.clean_headman()

        super().save(*args, **kwargs)
