from django.contrib import admin

from groups.models import Classroom, Group

from students.models import Student


class StudentTable(admin.TabularInline):
    model = Student
    fields = ['first_name', 'last_name', 'phone_number']
    readonly_fields = fields
    show_change_link = True


class GroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'rating', 'headman']
    fields = ['name', 'rating', 'headman', 'classrooms']
    inlines = [StudentTable]
    list_select_related = ['headman']


admin.site.register(Classroom)
admin.site.register(Group, GroupAdmin)
