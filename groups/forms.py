from django import forms
from django.core.exceptions import ValidationError # noqa

from groups.models import Group


class GroupBaseForm(forms.ModelForm):
    class Meta:
        model = Group
        # fields = ['name', 'members', 'rating', 'teacher', 'headman']
        fields = '__all__'
        exclude = ['start_date', 'end_date']


class GroupCreateForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        fields = '__all__'
        exclude = ['start_date', 'end_date']


class GroupEditForm(GroupBaseForm):
    class Meta(GroupBaseForm.Meta):
        fields = '__all__'
        exclude = ['start_date', 'end_date']
