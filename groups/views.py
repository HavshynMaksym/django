from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect # noqa
from django.shortcuts import get_object_or_404, render # noqa
from django.urls import reverse, reverse_lazy # noqa
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from groups.forms import GroupCreateForm, GroupEditForm
from groups.models import Group
from students.utils import format_list # noqa


# def get_groups(request):
#     groups = Group.objects.select_related('teacher').all()
#
#     params = [
#         'name',
#         'members',
#         'rating',
#     ]
#
#     for param in params:
#         value = request.GET.get(param)
#         if value:
#             groups = groups.filter(**{param: value})
#
#     return render(
#         request=request,
#         template_name='groups.html',
#         context={
#             'groups': groups
#         }
#     )
#
#
# def create_groups(request):
#
#     if request.method == 'GET':
#
#         form = GroupCreateForm()
#
#     elif request.method == 'POST':
#
#         form = GroupCreateForm(request.POST)
#
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('groups:list'))
#
#     return render(
#         request=request,
#         template_name='group_create.html',
#         context={
#             'form': form
#         }
#     )
#
#
# def edit_groups(request, id): # noqa
#     try:
#         group = Group.objects.get(id=id)
#     except Group.DoesNotExist:
#         return HttpResponse("Groups doesn't exist", status=404)
#
#     if request.method == 'GET':
#
#         form = GroupCreateForm(instance=group)
#
#     if request.method == 'POST':
#
#         form = GroupCreateForm(
#             data=request.POST,
#             instance=group
#         )
#
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('groups:list'))
#
#     return render(
#         request=request,
#         template_name='group_edit.html',
#         context={
#             'form': form,
#             'group': group,
#             'students': group.students.all(),
#
#         }
#     )
#
#
# def delete_group(request, id): # noqa
#     group = get_object_or_404(Group, id=id)
#
#     group.delete()
#     return HttpResponseRedirect(reverse('groups:list'))


class GroupUpdateView(LoginRequiredMixin, UpdateView):
    model = Group
    form_class = GroupEditForm
    template_name = 'group_edit.html'
    success_url = reverse_lazy('groups:list')
    context_object_name = 'group'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['students'] = self.get_object().students.all()
        return context

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Group successfully updated')
        return result


class GroupCreateView(LoginRequiredMixin, CreateView):
    model = Group
    form_class = GroupCreateForm
    template_name = 'group_create.html'
    success_url = reverse_lazy('groups:list')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Group successfully created')
        return result


class GroupListView(LoginRequiredMixin, ListView):
    model = Group
    template_name = 'groups.html'
    context_object_name = 'groups'
    paginate_by = 10

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.select_related('teacher').all()
        request = self.request

        params = [
            'name',
            'members',
            'rating',
        ]

        for param in params:
            value = request.GET.get(param)
            if value:
                qs = qs.filter(**{param: value})

        return qs


class GroupDeleteView(LoginRequiredMixin, DeleteView):
    model = Group
    success_url = reverse_lazy('groups:list')
    pk_url_kwarg = 'id'

    def get_object(self):
        obj = super().get_object()
        request = self.request
        messages.success(request, 'Group successfully deleted')
        return obj
