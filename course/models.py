from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=64)
    kind = models.CharField(max_length=64)

    def __str__(self):
        return f'{self.name}; {self.kind};'


class Technology(models.Model):
    name = models.CharField(max_length=64)
    version = models.CharField(max_length=32)
    course = models.ManyToManyField(
        to=Course,
        related_name='technologies'
    )

    def __str__(self):
        return f'{self.name}; {self.version};'
