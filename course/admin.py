from course.models import Course, Technology

from django.contrib import admin

admin.site.register(Course)
admin.site.register(Technology)
