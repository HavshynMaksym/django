from django.contrib import admin

# Register your models here.
from students.models import Student


class StudentAdmin(admin.ModelAdmin):
    exclude = ['uuid', 'create_date', 'published_date']
    list_per_page = 20


admin.site.register(Student, StudentAdmin)
