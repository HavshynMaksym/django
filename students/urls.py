from django.urls import path

from students.views import (
    # create_students,
    # delete_student,
    # edit_student,

    StudentCreateView,
    StudentDeleteView,
    StudentListView,
    StudentUpdateView,
    generate_students

)

app_name = 'students'

urlpatterns = [
    path('', StudentListView.as_view(), name='list'),
    path('generate/', generate_students, name='generate'),
    path('create/', StudentCreateView.as_view(), name='create'),
    path('edit/<uuid:uuid_code>', StudentUpdateView.as_view(), name='edit'),
    path('delete/<uuid:uuid_code>', StudentDeleteView.as_view(), name='delete'),
]
