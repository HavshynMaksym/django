from django import forms
from django.core.exceptions import ValidationError # noqa

from students.models import Student


class StudentBaseForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'phone_number', 'rating', 'group']

    # def clean_phone_number(self):
    #     phone_number = self.cleaned_data['phone_number']
    #     phone_number = str(phone_number)
    #     if not phone_number.isdigit():
    #         raise ValidationError("Write int format")
    #     return phone_number


class StudentCreateForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        fields = ['first_name', 'last_name', 'phone_number', 'rating', 'group']


class StudentEditForm(StudentBaseForm):
    class Meta(StudentBaseForm.Meta):
        fields = ['first_name', 'last_name', 'phone_number', 'rating', 'group']
