import datetime # noqa
import random
import uuid

from core.models import Person

from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from faker import Faker

from groups.models import Group


class Student(Person):
    create_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)
    rating = models.SmallIntegerField(null=True, default=0)
    phone_number = models.CharField(null=True, blank=True, max_length=20)
    uuid_code = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    group = models.ForeignKey(
        to=Group,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return f'{super().__str__()}, Rating: {self.rating}'

    def save(self, *args, **kwargs):

        phone_number = self.phone_number
        if phone_number is not None:
            phone_number = str(phone_number)
            if not phone_number.isdigit():
                raise ValidationError("Write int format")

        super().save(*args, **kwargs)

    @classmethod
    def create_student(cls):
        fake = Faker()

        first_name = fake.first_name()
        last_name = fake.last_name()
        birthdate = fake.date_of_birth()
        rating = random.randint(0, 100)
        groups = Group.objects.only('id').all()
        random_group = random.choice(groups)
        student = Student.objects.create(
            first_name=first_name,
            last_name=last_name,
            birthdate=birthdate,
            rating=rating,
            group=random_group)
        return student
