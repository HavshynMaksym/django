# Generated by Django 3.1 on 2020-09-18 13:36

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0022_auto_20200917_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='create_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 18, 13, 36, 2, 349479, tzinfo=utc)),
        ),
    ]
