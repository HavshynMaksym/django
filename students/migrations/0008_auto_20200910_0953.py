# Generated by Django 3.1 on 2020-09-10 09:53

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0007_auto_20200903_1943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='create_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 9, 10, 9, 53, 15, 471500, tzinfo=utc)),
        ),
    ]
