import random

from django.utils import timezone

from faker import Faker

from students.models import Student


def gen_password(length):
    result = ''.join([
        str(random.randint(0, 9))
        for _ in range(length)
    ])
    return result


def parse_length(request, default=10):
    length = request.GET.get('length', str(default))

    if not length.isnumeric():
        raise ValueError("VALUE ERROR: int")

    length = int(length)

    if not 3 < length < 100:
        raise ValueError("RANGE ERROR: [3..10]")

    return length


def parse_count(request):
    count = request.GET.get('count')
    if count is None:
        raise ValueError("VALUE ERROR: Add count")
    if not count.isnumeric():
        raise ValueError("VALUE ERROR: int")

    count = int(count)

    if not 0 <= count <= 100:
        raise ValueError("RANGE ERROR: [0 - 100]")

    return count


def create_random_students(count=10):
    fake = Faker()
    for _ in range(count):
        first_name = fake.first_name()
        last_name = fake.last_name()
        birthdate = fake.date_of_birth()
        rating = random.randint(0, 100)
        # students = Student(first_name=first_name, last_name=last_name, birthdate=birthdate)
        student = Student.objects.create(first_name=first_name, last_name=last_name, birthdate=birthdate, rating=rating)
        # students.save()
        student.publish()
    return Student.objects.filter(published_date__lte=timezone.now())


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)
