from django.core.management.base import BaseCommand

from students.models import Student


class Command(BaseCommand):
    help = 'Create random students' # noqa

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        for _ in range(options['count']):
            students = Student.create_student()

        self.stdout.write(str(students))
