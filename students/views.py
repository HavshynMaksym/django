from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect # noqa
from django.shortcuts import get_object_or_404, render # noqa
from django.urls import reverse, reverse_lazy # noqa
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from students.forms import StudentCreateForm, StudentEditForm
from students.models import Student
from students.utils import create_random_students, format_list, gen_password, parse_count, parse_length
# Create your views here.


def hello(request):
    return HttpResponse('Hello from Django!')


def get_random(request):
    try:
        length = parse_length(request, 10)
    except Exception as ex:
        return HttpResponse(str(ex), status_code=400)

    result = gen_password(length)

    return HttpResponse(result)


def generate_students(request):
    try:
        count = parse_count(request)
    except Exception as ex:
        return HttpResponse(str(ex), status=400)

    result = create_random_students(count)

    return HttpResponse(format_list(result))


# def get_students(request):
#     students = Student.objects.select_related('group').all()
#
#     params = [
#         'first_name',
#         'last_name',
#         'rating',
#         'rating__gt',
#         'rating__lt',
#     ]
#
#     for param in params:
#         value = request.GET.get(param)
#         if value:
#             students = students.filter(**{param: value})
#
#     return render(
#         request=request,
#         template_name='students.html',
#         context={
#             'students': students,
#
#         }
#     )


# def create_students(request):
#
#     if request.method == 'GET':
#
#         form = StudentCreateForm()
#
#     elif request.method == 'POST':
#
#         form = StudentCreateForm(request.POST)
#
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('students:list'))
#
#     return render(
#         request=request,
#         template_name='students_create.html',
#         context={
#             'form': form,
#
#         }
#     )


# def edit_student(request, uuid_code):
#     try:
#         student = Student.objects.get(uuid_code=uuid_code)
#     except Student.DoesNotExist:
#         return HttpResponse("Students doesn't exist", status=404)
#
#     if request.method == 'GET':
#
#         form = StudentEditForm(instance=student)
#
#     if request.method == 'POST':
#
#         form = StudentEditForm(
#             data=request.POST,
#             instance=student
#         )
#
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse('students:list'))
#
#     return render(
#         request=request,
#         template_name='students_edit.html',
#         context={
#             'form': form,
#             'student': student,
#
#         }
#     )


# def delete_student(request, uuid_code):
#     # try:
#     #     student = Student.objects.get(uuid_code=uuid_code)
#     # except Student.DoesNotExist:
#     #     return HttpResponse("Students doesn't exist", status=404)
#
#     student = get_object_or_404(Student, uuid_code=uuid_code)
#
#     student.delete()
#     return HttpResponseRedirect(reverse('students:list'))


class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Student
    form_class = StudentEditForm
    template_name = 'students_edit.html'
    success_url = reverse_lazy('students:list')
    context_object_name = 'student'
    pk_url_kwarg = 'uuid_code'

    def get_object(self):
        uuid_code = self.kwargs.get('uuid_code')
        return self.get_queryset().get(uuid_code=uuid_code)

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Student successfully updated')
        return result


class StudentCreateView(LoginRequiredMixin, CreateView):
    model = Student
    form_class = StudentCreateForm
    template_name = 'students_create.html'
    success_url = reverse_lazy('students:list')

    def form_valid(self, form):
        result = super().form_valid(form)
        request = self.request
        messages.success(request, 'Student successfully created')
        return result


class StudentListView(LoginRequiredMixin, ListView):
    model = Student
    template_name = 'students.html'
    context_object_name = 'students'
    paginate_by = 10

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.select_related('group').all()
        request = self.request

        params = [
            'first_name',
            'last_name',
            'rating',
            'rating__gt',
            'rating__lt',
        ]

        for param in params:
            value = request.GET.get(param)
            if value:
                qs = qs.filter(**{param: value})

        return qs


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Student
    success_url = reverse_lazy('students:list')
    pk_url_kwarg = 'id'

    def get_object(self):
        uuid_code = self.kwargs.get('uuid_code')
        request = self.request
        messages.success(request, 'Student successfully deleted')
        return self.get_queryset().get(uuid_code=uuid_code)
