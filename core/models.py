import datetime

from django.core.exceptions import ValidationError # noqa
from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True
    create_date = models.DateTimeField(auto_now_add=True, null=True)
    write_date = models.DateTimeField(auto_now_add=True, null=True)

    def save(self, *args, **kwargs):

        self.write_date = datetime.datetime.now()

        super().save(*args, **kwargs)


class Person(BaseModel):
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=64, null=False)
    last_name = models.CharField(max_length=64, null=False)
    birthdate = models.DateTimeField(null=True, default=datetime.date.today)

    def age(self):
        return datetime.datetime.now().date().year - self.birthdate.year

    def __str__(self):
        return f'{self.id}) ' \
               f'Name: {self.first_name};' \
               f' Surname: {self.last_name};' \
               f'Age: {self.age()};'
